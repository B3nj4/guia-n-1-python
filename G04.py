#Con for
N=int(input("Ingrese la cantidad de alumnos: "))
for i in range (1,N+1):
    print("\n///////////////Persona",i,"///////////////")
    edad=int(input("Ingrese la edad de esta persona: "))
    
    if i == 1:
        edadanterior = edad
        edadtotal = edadanterior
    
    elif i>1 and i<N:
        edadtotal = edadtotal + edad

    elif i == N:
        edadtotal = edadtotal + edad
        promedio = edadtotal/N 
        print("\nEl promedio de las edades del grupo es:",promedio)
        print("\n")

#Con While
N=int(input("Ingrese la cantidad de alumnos: "))
i=1
while i < N+1:
    print("\n///////////////Persona",i,"///////////////")
    edad=int(input("Ingrese la edad de esta persona: "))

    if i == 1:
        edadanterior = edad
        edadtotal = edadanterior
    
    elif i>1 and i<N:
        edadtotal = edadtotal + edad

    elif i == N:
        edadtotal = edadtotal + edad
        promedio = edadtotal/N 
        print("\nEl promedio de las edades del grupo es:",promedio)
        print("\n")
    i = i+1
    