N=int(input("Ingrese el número de personas que postularon al trabajo: "))   #Planteo de la variable N ,es decir, el numero de personas que postularon

for i in range (1,N+1):  #For para recorrer del 1 hasta el N (N+1 para considerar N,e xplicado de otra forma,todos los numeros menores a N+1 entre esos el N)
    print("\n////////////////////Postulante",i,"/////////////////////////")
    if i == 1: #Para el primer postulante, como no tiene la edad de un postulante anterior a este, tendra una diferente esctructura al resto 
        Año=int(input("\nIngrese el año de nacimiento del postulante: "))
        Edad = 2020 - Año #para calcular la edad use el año actual (2020) restado por el año de nacimiento de la persona
        print("La edad del postulante es:",Edad)
        Edadanterior = Edad   #edad anterior es igual a la edad i=1 porque ahora pasa a ser la edad anterior del postulante 2
        Sumaedad = Edadanterior #en este caso no se tiene con que sumar las edades para calcular el promedio
                
    elif i>1 and i<N:
        Año=int(input("\nIngrese el año de nacimiento del postulante: "))
        Edad = 2020 - Año
        print("La edad del postulante es:",Edad)
        Sumaedad = Sumaedad + Edad #se van sumando las edades para sacar el promedio, se suma la suma de edades anteriores con la edad del postulante i
        Edadanterior = Edad #defino las edades actuales como edades anteriores para los siguientes postulantes

    elif i == N: #el ultimo postulante es diferente porque imprimirá el promedio
        Año=int(input("\nIngrese el año de nacimiento del postulante: "))
        Edad = 2020 - Año
        print("La edad del postulante es:",Edad)
        Sumaedad = Sumaedad + Edad
        Promedio = Sumaedad/N #promedio de las edades es igual a la sumatorioa de las edades dividido en la cantidad de postulantes
        print("\n-----> El promedio de las edades de los postulantes al trabajo corresponde a: ",Promedio," <-----\n")
